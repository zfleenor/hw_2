package vivz.slidenerd.hw2testnew;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends Activity {

    //Initialize position in String names array = 0 so that it starts from 0, which is the first
    //item in the list
    int pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        //Set up getIntent so that it receives the call from CustomAdapter
        Intent i = getIntent();
        pos = i.getExtras().getInt("Position");

        //Initialize the array adapter, the image, and the description text
        final CustomAdapter adapter = new CustomAdapter(this);
        final ImageView img = (ImageView) findViewById(R.id.imageView);
        final TextView nameTxt_2 = (TextView) findViewById(R.id.textView2);

        nameTxt_2.setTextSize(16);
        nameTxt_2.setTypeface(null, Typeface.BOLD);

        //Get position in String array to display its according image and description
        img.setImageResource(adapter.images[pos]);
        nameTxt_2.setText(adapter.description[pos]);

    }

    //Main to fragment method
    public static void onMsgFromMainToFragment(String s) {
    }
}
