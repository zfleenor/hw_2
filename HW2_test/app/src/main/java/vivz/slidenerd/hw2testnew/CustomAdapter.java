package vivz.slidenerd.hw2testnew;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

class CustomAdapter extends BaseAdapter {


    public Context c;

    //String of location names in the Text View
    public String[] names = {"Amarillo, Texas", "Lubbock, Texas", "Fort Worth, Texas",
            "Breckenridge, Texas", "May, Texas", "Chillicothe, Texas"};

    //Image resources for each location
    public int[] images = {R.drawable.amarillo_location, R.drawable.lubbock_location, R.drawable.fortworth_location,
            R.drawable.breckenridge_location, R.drawable.may_location, R.drawable.chillicothe_location};

    //Detail description of each location when each button is clicked
    public String[] description = {
            "\n" +
            "Amarillo is the 14th-most populous city in the state of Texas.\n" +
                    "Population: 190,695\n\n" +
                    "Fun Fact: I was born here January 18th, 1994.",
            "Lubbock, Texas is the 83rd most populous city in the United States and the 11th in the state of Texas.\n\n" +
                    "Population: 249,042\n\n" +
                    "Nickname: \"Hub City\n\n" +
                    "Fun Fact: I lived in Lubbock from 2000-2004, then moved off to other parts of Texas only to return in 2012 to pursues my degrees in Computer/Electrical Engineering at Texas Tech University.\n",
            "Fort Worth is the 16th-largest city in the United States and the fifth-largest city in the state of Texas.\n\n" +
                    "Population: 833,319\n\n" +
                    "Fun Fact: I attended 5th and 6th grade here and learned how to do a rubiks cube.",
            "Breckenridge is a city in Stephens County, Texas, United States.\n\n" +
                    "Population: 5,780\n\n" +
                    "Fun Fact: My mother, me, and my 4 younger siblings hid under a matress in our downstairs closet while 3 \"sister\" tornadoes touched down throughout the city.\n",
            "May is an unincorporated community in Brown County, Texas, United States.\n\n" +
                    "Population: 285\n\n " +
                    "Fun Fact: My dog Doug jumped into the back of my pickup in 2010 and has been mine ever since.",
            "Chillicothe is a city in Hardeman County, in the U.S. state of Texas.\n\n" +
                    "Population: 707\n\n" +
                    "UnFun Fact: I graduated 3rd in my class of 12, with a GPA of 4.29, while 1/12 had a 3.67, because I transfered between my junior and senior year, they would not allow me to graduate first or second\n"};


    //Context for the adapter
    public CustomAdapter(Context ctx) {
        this.c = ctx;
    }

    //Return length of location names
    @Override
    public int getCount() {
        return names.length;
    }

    //Return the location name in the list view
    @Override
    public Object getItem(int pos) {
        return names[pos];
    }

    //Return the position of the location in the list view
    @Override
    public long getItemId(int pos) {
        return pos;
    }

    //Creating the View
    @Override
    public View getView(final int pos, View convertView, ViewGroup parent) {


        //Using inflater method to create a rows of list view from one single row layout in single_row.xml
        //single_row.xml has an ImageView, a TextView for the location names, and a Button for description detail

        LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        final TextView location_txt = (TextView) ((MainActivity) c).findViewById(R.id.textView4);
        location_txt.setTypeface(null, Typeface.BOLD_ITALIC);
        location_txt.setTextSize(18);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.single_row, parent, false);
        }


        TextView nameTxt = (TextView) convertView.findViewById(R.id.textView);

        //If detail_button is clicked, go to DetailActivity class, which displays the activity_detail.xml
        Button myDescription = (Button) convertView.findViewById(R.id.detail_button);
        myDescription.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                //Set up Intent to call DetailActivity to open activity_detail layout
                Intent j = new Intent(c.getApplicationContext(), DetailActivity.class);
                j.putExtra("Position", pos);
                c.startActivity(j);
                location_txt.setText("Previous chosen location: " + names[pos]);
            }
        });


        //If an image is clicked, go to ExpandActivity class, which display the expanded version of
        //the image
        ImageView img = (ImageView) convertView.findViewById(R.id.imageView);
        img.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //Set up different Intent to call ExpandActivity to open activity_expand layout
                Intent i = new Intent(c.getApplicationContext(), ExpandActivity.class);
                i.putExtra("Position", pos);
                c.startActivity(i);
                location_txt.setText("Previous chosen location: " + names[pos]);
            }
        });

        //Get position in String array to display location name and its according image
        nameTxt.setText(names[pos]);
        img.setImageResource(images[pos]);


        return convertView;
    }


}

