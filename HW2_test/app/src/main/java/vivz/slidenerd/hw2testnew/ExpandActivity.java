package vivz.slidenerd.hw2testnew;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

public class ExpandActivity extends Activity {

    //Initialize position in String names array = 0 so that it starts from 0, which is the first
    //item in the list
    int pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expand);

        //Set up getIntent so that it receives the call from CustomAdapter
        Intent i = getIntent();
        pos = i.getExtras().getInt("Position");

        //Initialize the array adapter and the expanded image
        final CustomAdapter adapter = new CustomAdapter(this);
        final ImageView img = (ImageView) findViewById(R.id.imageView);

        //Get position in String array to display its according image
        img.setImageResource(adapter.images[pos]);

    }

    //Main to fragment method
    public static void onMsgFromMainToFragment(String s) {
    }
}
