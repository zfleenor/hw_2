package vivz.slidenerd.hw2testnew;


// methods to pass messages from MainActivity to Fragments
public interface FragmentCallbacks {
    void onMsgFromMainToFragment(String strValue);

}