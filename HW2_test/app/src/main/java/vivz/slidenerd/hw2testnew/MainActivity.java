package vivz.slidenerd.hw2testnew;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;


public class MainActivity extends Activity implements MainCallbacks {

    //Initialize the list view
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create an instance of the list view, the array adapter
        lv = (ListView) findViewById(R.id.listView1);
        CustomAdapter adapter = new CustomAdapter(this);
        lv.setAdapter(adapter);


    }


    // Fragment to main method
    @Override
    public void onMsgFromFragToMain(String sender, String strValue) {

        if (sender.equals("RED-FRAG")) {

        }

        if (sender.equals("BLUE-FRAG")) {
            try {

                ExpandActivity.onMsgFromMainToFragment("\nSender: " + sender + "\nMsg: " + strValue);
                DetailActivity.onMsgFromMainToFragment("\nSender: " + sender + "\nMsg: " + strValue);
            } catch (Exception e) {
                Log.e("ERROR", "onStrFromFragToMain " + e.getMessage());
            }
        }

    }
}



